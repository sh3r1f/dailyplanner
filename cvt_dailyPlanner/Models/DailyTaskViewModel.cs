﻿using System;
using System.Globalization;
using cvt_dailyPlanner.Data.Interface;

namespace cvt_dailyPlanner.Models
{
    public class DailyTaskViewModel : IDailyTask
    {
        public int Id { get; set; }
        public string Sub { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsDone { get; set; }
        public string Place { get; set; }
        public string TaskType { get; set; }
    }
}
