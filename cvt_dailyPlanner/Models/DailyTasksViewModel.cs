﻿using System;
using System.Collections.Generic;
using cvt_dailyPlanner.Data.Interface;

namespace cvt_dailyPlanner.Models
{
    public class DailyTasksViewModel
    {
        public IEnumerable<DailyTaskViewModel> dailyTasks { get; set; }
        public FilterViewModel filter { get; set; }
    }
}
