﻿using System;
namespace cvt_dailyPlanner.Models
{
    public class FilterViewModel
    {
        public bool taskTypeChecked { get; set; }
        public string DateType { get; set; }
        public string dateUp { get; set; }
        public string dateDown { get; set; }
        public bool DateTypeFilter { get; set; }
        public string TaskType { get; set; }
        public bool selectDateFilter { get; set; }
        public string SelectDate { get; set; }
    }
}
