﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DailyTasks](
	[Id] [int] IDENTITY,
    [StartDate] [DateTime] NOT NULL,
    [Sub] [NVARCHAR](100) NOT NULL,
    [EndDate] [DateTime],
    [Place] [NVARCHAR](100),
    [TaskType] [NVARCHAR](20),
    [IsDone] [Bit]
) ON [PRIMARY]
GO