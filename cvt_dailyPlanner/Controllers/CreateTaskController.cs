﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using cvt_dailyPlanner.Data.Interface.IRepository;
using cvt_dailyPlanner.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace cvt_dailyPlanner.Controllers
{
    public class CreateTaskController : Controller
    {
        public readonly IRepository _repository;
        private readonly ILogger<CreateTaskController> _logger;
        public CreateTaskController(IRepository repository, ILogger<CreateTaskController> logger)
        {
            _repository = repository;
            _logger = logger;
        }
        // GET: /<controller>/
        public IActionResult Index(string date)
        {
            ViewBag.date = date;
            return View();
        }

        public IActionResult Create (DailyTaskViewModel dtv, string StartDate, string EndDate)
        {
            _logger.LogInformation("Начали создавть задачу");
            try
            {
                CultureInfo culture = new CultureInfo("ru-RU");
                dtv.StartDate = DateTime.Parse(StartDate, culture);
                if (!string.IsNullOrEmpty(EndDate))
                    dtv.EndDate = DateTime.Parse(EndDate, culture);

                _repository.CreateTask(dtv);
                
            }
            catch (Exception ex)
            {
                _logger.LogError("Ошибка создания задачи", ex);
            }
            _logger.LogInformation("Задача созданна");
            return RedirectToRoute(new { controller = "Planner", action="Index"});
        }
    }
}
