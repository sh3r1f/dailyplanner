﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using cvt_dailyPlanner.Data.Interface;
using cvt_dailyPlanner.Data.Interface.IRepository;
using cvt_dailyPlanner.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace cvt_dailyPlanner.Controllers
{
    public class PlannerController : Controller
    {

        public readonly IRepository _repository;
        private readonly ILogger<HomeController> _logger;

        public PlannerController(IRepository repository, ILogger<HomeController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        // GET: /<controller>/
        public IActionResult Index(DailyTasksViewModel dtv)
        {
            DailyTasksViewModel obj = dtv ?? new DailyTasksViewModel();
            var tasks = _repository.GetAllDailyPlan;
            obj.dailyTasks = tasks.OrderBy(t => t.StartDate);
            //ViewDate["period"] = "Index";
            return View(obj);
        }

        public IActionResult Week()
        {
            DailyTasksViewModel obj = new DailyTasksViewModel();
            obj.dailyTasks = _repository.WeekTask.GetActualyWeekPlan;
            //ViewBag["period"] = "Week";
            return View(obj);
        }

        public IActionResult Find(string Sub)
        {
            DailyTasksViewModel obj = new DailyTasksViewModel();
            try
            {
                var tasks = _repository.GetAllDailyPlan;
                obj.dailyTasks = tasks.Where(t=> t.Sub.Contains(Sub));
            }
            catch(Exception ex)
            {
                obj.dailyTasks = null;
                _logger.LogError("Не получилось найти задание", ex);
            }

            return View("Index", obj);
        }

        public IActionResult Month(bool isAllTask)
        {
            DailyTasksViewModel obj = new DailyTasksViewModel();
            var tasks = _repository.MonthTask.GetActualyMonthPlan;
            obj.dailyTasks = tasks.OrderBy(t=> t.StartDate);
            if (isAllTask)
            {
                DateTime today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                obj.filter = new FilterViewModel();
                obj.filter.SelectDate = string.Format("{0} - {1}",
                    today.ToString("dd.MM.yyyy"),
                    today.AddMonths(1).ToString("dd.MM.yyyy"));
                obj.filter.DateTypeFilter = true;
                obj.filter.selectDateFilter = true;
                return View("Index", obj);
            }
            else
            {
                return View(obj);
            }
            
        }


        public IActionResult Day(string todayDay)
        {
            CultureInfo culture = new CultureInfo("ru-RU");

            DailyTasksViewModel obj = new DailyTasksViewModel();
            if(!string.IsNullOrEmpty(todayDay))
            {
                DateTime date = DateTime.Parse(todayDay, culture);
                obj.dailyTasks = _repository.DailyTask.GetDailyPlanByDay(date);
            }
                
            else
                obj.dailyTasks = _repository.DailyTask.GetTodayPlan;

            ViewBag.period = "Day";
            ViewBag.date = todayDay ?? DateTime.Now.ToString("dd.MM.yyyy");
            return View(obj);
        }

        public IActionResult Task(int id)
        {
            IDailyTask obj = _repository.getTaskById(id);
            return View(obj);
        }

        public IActionResult Update(DailyTaskViewModel dtv, string period)
        {
            _logger.LogInformation("Начали создавть задачу");
            try
            {
                _repository.UpdateTask(dtv);
            }
            catch (Exception ex)
            {
                _logger.LogError("Ошибка создания задачи", ex);
            }
            _logger.LogInformation("Задача созданна");
            return View("Task", dtv);
        }

        public IActionResult Filter(DailyTasksViewModel dtv)
        {
            var task = _repository.GetAllDailyPlan;

            if (!string.IsNullOrEmpty(dtv.filter.TaskType))
                task = task.Where(t=> t.TaskType == dtv.filter.TaskType);

            if (dtv.filter.DateType == "down")
                task = task.OrderByDescending(t => t.StartDate);
            else
                task = task.OrderBy(t => t.StartDate);

            if (!string.IsNullOrEmpty(dtv.filter.SelectDate))
            {
                var selectDates = dtv.filter.SelectDate.Split('-');
                CultureInfo culture = new CultureInfo("ru-RU");
                DateTime startDate = DateTime.Parse(selectDates[0], culture);
                DateTime endDate = DateTime.Parse(selectDates[1], culture);
                task = task.Where(t => t.StartDate >= startDate && t.StartDate <= endDate).ToList();
            }

            dtv.dailyTasks = task;
            return View("Index", dtv);
        }

        [HttpPost]
        public bool Done(int id)
        {
            DailyTaskViewModel dtv = _repository.getTaskById(id);
            _logger.LogInformation("Выполняем задачу");
            try
            {
                dtv.IsDone = !dtv.IsDone;
                _repository.UpdateTask(dtv);
            }
            catch(Exception ex)
            {
                _logger.LogDebug("Не удалось выполнить задачу", ex);
                dtv.IsDone = !dtv.IsDone;
                return false;
            }
            
            _logger.LogInformation("Выполнили задачу");
            return true;
        }
    }
}
