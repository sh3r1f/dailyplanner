﻿$(document).ready(function () {
    $('.single-data-time-picker').daterangepicker({
        singleDatePicker: true,
        timePicker: true,
        locale: {
            format: 'DD.MM.YYYY hh:mm A'
        }
    });

    $('.data-time-picker').daterangepicker({
        singleDatePicker: false,
        timePicker: false,
        locale: {
            format: 'DD.MM.YYYY'
        }
    });
});

async function setIsDone(obj, id) {
    $.ajax(
        {
            type: 'POST',
            url: 'Done',
            data: {'id':id},
            success: function (data) {
                if (data) {
                    debugger;
                    if (obj.classList.contains('done')) {
                        obj.classList.remove('done');
                    }
                    else {
                       obj.classList.add('done');
                    }
                }
            }
        });

    


}

function VisibleFilter(obj) {
    var el = $('.filter-task');
    debugger;
    if (el.is(':visible'))
        el.hide();
    else
        el.show();


    let dateInp = $('.filterDateType');
    let periodFilter = $('#period-block-filter');
    let taskTypeChec = $('.taskTypeFilter');

    var ft = obj.filter;
    if (ft !== null) {
        if (ft.dateTypeFilter) {
            dateInp.prop('checked', true);

            debugger;
            if (ft.dateType == "up")
                $('input[name="filterTask.DateType"][value="up"]').prop('checked', true);
            else
                $('input[name="filterTask.DateType"][value="down"]').prop('checked', true);

            if (ft.selectDateFilter) {
                periodFilter.prop('checked', true);
                $('#period-date').val(ft.selectDate);

            }

        }

        if (ft.taskTypeChecked) {
            taskTypeChec.prop('checked', true);
            $('task-type').val(ft.taskType);
        }
    }

    ChangeDateFilter(dateInp, dateInp.is(':checked'));
    ChangeDatePeriodFilter(periodFilter, periodFilter.is(':checked'));
    ChangeTaskTypeFilter(taskTypeChec, taskTypeChec.is(':checked'));
}

function Selected(a) {

    let label = a.value;
    let place = $("#Place");
    let endDate = $("#EndDate");
    place.show();
    endDate.show();

    if (label !== 'Встреча') {
        place.hide();
    }

    if (label === 'Памятка') {
        endDate.hide();
    }
};

function ChangeTaskTypeFilter(a, check) {
    let el = $('.task-type-filter');

    if (a.checked || check) {
        el.show();
    }
    else {
        el.hide();
        $('task-type').val('');
    }
}

function ChangeDateFilter(a, check) {
    let el = $('.date-filter');

    if (a.checked || check) {
        el.show();
    }
    else {
        el.hide();
        $('#date-inp').prop('checked', false);
        el = $('#period-block-filter');
        el.prop('checked', false);
        ChangeDatePeriodFilter(el);
    }
}

function ChangeDatePeriodFilter(a, check) {
    let el = $('.period-filter');

    if (a.checked || check) {
        el.show();
    }
    else {
        el.hide();
        $('#period-date').val('');
    }
}