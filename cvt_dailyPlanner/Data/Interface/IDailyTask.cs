﻿using System;
namespace cvt_dailyPlanner.Data.Interface
{
    public interface IDailyTask
    {
        int Id { get; set; }
        string Sub { get; set; }
        DateTime StartDate { get; set; }
        DateTime? EndDate { get; set; }
        bool IsDone { get; set; }
        string Place { get; set; }
        string TaskType { get; set; }
    }
}
