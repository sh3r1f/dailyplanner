﻿using System;
using System.Collections.Generic;
using cvt_dailyPlanner.Models;

namespace cvt_dailyPlanner.Data.Interface.IRepository
{
    public interface IDailyRepositories 
    {
        //Получить список задач на день
        IEnumerable<DailyTaskViewModel> GetDailyPlanByDay(DateTime date);

        //Получить задачи за сегодня
        IEnumerable<DailyTaskViewModel> GetTodayPlan { get; }
    }
}
