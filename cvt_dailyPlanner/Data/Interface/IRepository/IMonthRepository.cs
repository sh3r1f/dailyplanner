﻿using System;
using System.Collections.Generic;
using cvt_dailyPlanner.Models;

namespace cvt_dailyPlanner.Data.Interface.IRepository
{
    public interface IMonthRepository
    {
        //Получить все задачи за месяц
        IEnumerable<DailyTaskViewModel> GetDailyPlanByMonth(int month);

        //Получить список задач за текующий месяц
        IEnumerable<DailyTaskViewModel> GetActualyMonthPlan { get; }
    }
}
