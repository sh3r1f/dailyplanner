﻿using System;
using System.Collections.Generic;
using cvt_dailyPlanner.Models;

namespace cvt_dailyPlanner.Data.Interface.IRepository
{
    public interface IWeekRepository
    {
        //Получить все задачи за неделю
        IEnumerable<DailyTaskViewModel> GetDailyPlanByWeek(DateTime date);

        //Получить список задач за текующую неделю
        IEnumerable<DailyTaskViewModel> GetActualyWeekPlan { get; }
    }
}
