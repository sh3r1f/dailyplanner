﻿using System;
using System.Collections.Generic;
using cvt_dailyPlanner.Models;

namespace cvt_dailyPlanner.Data.Interface.IRepository
{
    public interface IRepository
    {
        IDailyRepositories DailyTask { get; }
        IWeekRepository WeekTask { get; }
        IMonthRepository MonthTask { get; }
        //получить все задачи
        IEnumerable<DailyTaskViewModel> GetAllDailyPlan { get; }
        //Создать задачу
        void CreateTask(DailyTaskViewModel dt);
        //Обновить информацию по задаче
        void UpdateTask(DailyTaskViewModel dt);
        //Получить задачу
        DailyTaskViewModel getTaskById(int id);
    }
}
