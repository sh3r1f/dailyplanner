﻿using System;
using cvt_dailyPlanner.Data.Interface;
using cvt_dailyPlanner.Models;
using Microsoft.EntityFrameworkCore;

namespace cvt_dailyPlanner.Data
{
    public class dbContent : DbContext
    {
        public dbContent(DbContextOptions<dbContent> options) : base(options)
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=localhost,1433\\Catalog=sql_server_demo;Database=DailyPlan;User=sa;Password=reallyStrongPwd123");
        }

        public DbSet<DailyTaskViewModel> DailyTasks { get; set; }
    }
}
