﻿using System;
using System.Collections.Generic;
using System.Linq;
using cvt_dailyPlanner.Data.Interface;
using cvt_dailyPlanner.Data.Interface.IRepository;
using cvt_dailyPlanner.Models;

namespace cvt_dailyPlanner.Data.Repository
{
    public class MonthRepository : IMonthRepository
    {
        private readonly dbContent _dbContent;
        public MonthRepository(dbContent content) => _dbContent = content;
        
        public IEnumerable<DailyTaskViewModel> GetActualyMonthPlan { get =>
                _dbContent.DailyTasks.Where(t =>
                        t.StartDate.Month == DateTime.Now.Month); }

        public IEnumerable<DailyTaskViewModel> GetDailyPlanByMonth(int month) =>
            _dbContent.DailyTasks.Where(t =>
                        t.StartDate.Month == month);
    }
}
