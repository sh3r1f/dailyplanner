﻿using System;
using System.Collections.Generic;
using System.Linq;
using cvt_dailyPlanner.Data.Interface;
using cvt_dailyPlanner.Data.Interface.IRepository;
using cvt_dailyPlanner.Models;

namespace cvt_dailyPlanner.Data.Repository
{
    public class DailyRepository : IDailyRepositories
    {
        public readonly dbContent _dbContent;

        public DailyRepository(dbContent dbContent) => _dbContent = dbContent;

        public IEnumerable<DailyTaskViewModel> GetTodayPlan => _dbContent.DailyTasks.Where(
            t => t.StartDate == DateTime.Now);

        public IEnumerable<DailyTaskViewModel> GetDailyPlanByDay(DateTime date) =>_dbContent.DailyTasks.Where(
            t => t.StartDate == date);

    }
}
