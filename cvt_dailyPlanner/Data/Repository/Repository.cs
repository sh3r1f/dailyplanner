﻿using System;
using System.Collections.Generic;
using System.Linq;
using cvt_dailyPlanner.Data.Interface;
using cvt_dailyPlanner.Data.Interface.IRepository;
using cvt_dailyPlanner.Models;

namespace cvt_dailyPlanner.Data.Repository
{
    public class Repository : IRepository
    {
        public IDailyRepositories DailyTask { get; }
        public IWeekRepository WeekTask { get; }
        public IMonthRepository MonthTask { get; }

        public readonly dbContent _dbContent;

        public Repository(dbContent dbContent)
        {
            _dbContent = dbContent;

            DailyTask = new DailyRepository(_dbContent);
            WeekTask = new WeekRepository(_dbContent);
            MonthTask = new MonthRepository(_dbContent);
        }

        public IEnumerable<DailyTaskViewModel> GetAllDailyPlan => _dbContent.DailyTasks;

        public void CreateTask(DailyTaskViewModel dt)
        {
            _dbContent.DailyTasks.Add(dt);
            _dbContent.SaveChanges();
        }

        public void UpdateTask(DailyTaskViewModel dt)
        {
            _dbContent.DailyTasks.UpdateRange(dt);
            _dbContent.SaveChanges();
        }

        public DailyTaskViewModel getTaskById(int id) => _dbContent.DailyTasks.FirstOrDefault(t=> t.Id == id);    
    }
}
