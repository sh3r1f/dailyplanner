﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using cvt_dailyPlanner.Data.Interface;
using cvt_dailyPlanner.Data.Interface.IRepository;
using cvt_dailyPlanner.Models;

namespace cvt_dailyPlanner.Data.Repository
{
    public class WeekRepository : IWeekRepository
    {
        private readonly dbContent _dbContent;
        public WeekRepository(dbContent content) => _dbContent = content;

        public IEnumerable<DailyTaskViewModel> GetActualyWeekPlan
        {
            get
            {
                return getPlan(DateTime.Now);
            }
        }

        public IEnumerable<DailyTaskViewModel> GetDailyPlanByWeek(DateTime date) => getPlan(date);

        private int getWeekOfYear(DateTime date)
        {
            int d1 = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
            return d1;
        }

        private IEnumerable<DailyTaskViewModel> getPlan (DateTime date)
        {
            var startDate = getWeekOfYear(date);
            IEnumerable<DailyTaskViewModel> tasks =  _dbContent.DailyTasks;
            return tasks.Where(t => getWeekOfYear(t.StartDate) == 7);
        }
    }
}
