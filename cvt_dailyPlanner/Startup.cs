using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cvt_dailyPlanner.Data;
using cvt_dailyPlanner.Data.Interface.IRepository;
using cvt_dailyPlanner.Data.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace cvt_dailyPlanner
{
    public class Startup
    {

        public IConfiguration _confString { get; }
        public Startup(IWebHostEnvironment hostEnv)
        {
            _confString = new ConfigurationBuilder().
                SetBasePath(hostEnv.ContentRootPath).AddJsonFile("Properties/dbConfigurationSettings.json").Build();
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddRazorPages();
            services.AddSession();
            services.AddDbContext<dbContent>(options =>
                options.UseSqlServer(_confString.GetConnectionString("DefaultConnection")));
            services.AddTransient<IRepository, Repository>();
            services.AddControllersWithViews();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseExceptionHandler("/Home/Error");
            //    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            //    app.UseHsts();
            //}
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();
            app.UseCors();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                //endpoints.MapControllerRoute(
                //    name: "Planner",
                //    pattern: "Planner/Index/{period?}",
                //    defaults: new { Controller = "Planner", Action = "Index" });
            });
        }
    }
}
